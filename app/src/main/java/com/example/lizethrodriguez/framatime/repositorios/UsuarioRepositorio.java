package com.example.lizethrodriguez.framatime.repositorios;

import android.content.Context;
import android.util.Log;

import com.example.lizethrodriguez.framatime.entidades.DaoSession;
import com.example.lizethrodriguez.framatime.entidades.FarmaTimeDB;
import com.example.lizethrodriguez.framatime.entidades.Usuario;
import com.example.lizethrodriguez.framatime.entidades.UsuarioDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lizeth Rodriguez on 21/11/2016.
 */

public class UsuarioRepositorio {

    private static final String TAG = "Usuario";

    private Context context;

    public UsuarioRepositorio(Context prContext){this.context = prContext;}

    public boolean guardarUsuario(Usuario prUsuario)
    {
        try{
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            UsuarioDao oUsuairoDao = oFarmaTimeDB.getUsuarioDao();
            oUsuairoDao.insert(prUsuario);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally{
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public Usuario iniciarSesion(String usuarioSesion, String contrasena){
        DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
        UsuarioDao oUsuarioDao = oFarmaTimeDB.getUsuarioDao();
        Usuario usuario = oUsuarioDao.queryBuilder().where(UsuarioDao.Properties.Correo.eq(usuarioSesion), UsuarioDao.Properties.Contra.eq(contrasena)).unique();
        return usuario;
    }

    public boolean guardarOActualizarUsuario(Usuario prUsuario){
        try{
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            UsuarioDao oUsuarioDao = oFarmaTimeDB.getUsuarioDao();
            oUsuarioDao.insertOrReplace(prUsuario);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally{
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public boolean guardarOActualizarUsuario(ArrayList<Usuario> prUsuarios){
        try{
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            UsuarioDao oUsuarioDao = oFarmaTimeDB.getUsuarioDao();
            oUsuarioDao.insertOrReplaceInTx(prUsuarios);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public boolean eliminarUsuario(Usuario prUsuario){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            UsuarioDao oUsuarioDao = oFarmaTimeDB.getUsuarioDao();
            oUsuarioDao.delete(prUsuario);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public boolean eliminarTodo(){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            UsuarioDao oUsuarioDao = oFarmaTimeDB.getUsuarioDao();
            oUsuarioDao.deleteAll();
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public List<Usuario> obtenerUsuario(){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            UsuarioDao oUsuarioDao = oFarmaTimeDB.getUsuarioDao();
            return oUsuarioDao.loadAll();
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return null;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public Usuario obtenerUsuario(Long prKey){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            UsuarioDao oUsuarioDao = oFarmaTimeDB.getUsuarioDao();
            return oUsuarioDao.load(prKey);
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return null;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }
}

