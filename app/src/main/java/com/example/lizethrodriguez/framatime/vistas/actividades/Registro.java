package com.example.lizethrodriguez.framatime.vistas.actividades;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Toast;

import com.example.lizethrodriguez.framatime.R;
import com.example.lizethrodriguez.framatime.entidades.Usuario;
import com.example.lizethrodriguez.framatime.repositorios.UsuarioRepositorio;

import java.util.regex.Pattern;

public class Registro extends AppCompatActivity {

    public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private AppCompatEditText et_nombre, et_correo, et_contra;
    private AppCompatButton btn_registrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        this.et_nombre = (AppCompatEditText)findViewById(R.id.et_nombre);
        this.et_correo = (AppCompatEditText)findViewById(R.id.et_correo);
        this.et_contra = (AppCompatEditText)findViewById(R.id.et_contra);
        this.btn_registrar = (AppCompatButton)findViewById(R.id.btn_registrar);

        this.btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarTextos()){
                    Usuario usuario = new Usuario();
                    usuario.setNombre(et_nombre.getText().toString());
                    usuario.setCorreo(et_correo.getText().toString());
                    usuario.setContra(et_contra.getText().toString());
                    UsuarioRepositorio repositorio = new UsuarioRepositorio(Registro.this);
                    if(repositorio.guardarUsuario(usuario)){
                        Toast.makeText(Registro.this, "Usuario registrardo correctamente", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        });
    }

    public boolean validarTextos(){
        boolean textosValidos = true;

        if(et_nombre.getText().toString().length() == 0){
            et_nombre.setError("Requerido");
            textosValidos = false;
        }

        if(!Pattern.matches(EMAIL_REGEX, et_correo.getText().toString())){
            et_correo.setError("Formato incorrecto");
            textosValidos = false;
        }

        if(et_contra.getText().toString().length() == 0){
            et_contra.setError("Requerido");
            textosValidos = false;
        }
        return textosValidos;
    }

}
