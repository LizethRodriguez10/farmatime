package com.example.lizethrodriguez.framatime.entidades;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by Lizeth Rodriguez on 28/11/2016.
 */

public class TratamientoDao extends AbstractDao<Tratamiento, Long> {

    public static final String TABLENAME = "TRATAMIENTO";



    public static class Properties{
        public final static Property TratamientoId = new Property(0, Long.class, "tratamientoId", true, "TRATAMIENTO_ID");
        public final static Property Nombre_tratamiento = new Property(1, String.class, "nombre_tratamiento", true, "NOMBRE_TRATAMIENTO");
        public final static Property Medicamento = new Property(2, String.class, "medicamento", true, "MEDICAMENTO");
        public final static Property Fecha_inicio = new Property(3, String.class, "fecha_inicio", true, "FECHA_INICIO");
        public final static Property Dias = new Property(4, String.class, "dias", true, "DIAS");
        public final static Property Horas = new Property(5, String.class, "horas", true, "HORAS");
        public final static Property UsuarioId = new Property(6, Long.class, "usuarioId", true, "USUARIO_ID");
    };

    private DaoSession daoSession;

    private Query<Tratamiento> usuario_TratamientosQuery;

    public TratamientoDao(DaoConfig config) {super(config);}


    public TratamientoDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists){
        String constraint = ifNotExists ? "IF NOT EXISTS" : "";
        db.execSQL("CREATE TABLE" + constraint + "'TRATAMIENTO' (" +
        "'TRATAMIENTO_ID' INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0; tratamientoId
        "'NOMBRE_TRATAMIENTO' TEXT," + //1; nombre_tratamiento
        "'MEDICAMENTO' TEXT," + //2; medicamento
        "'FECHA_INICIO' TEXT," + //3; fecha_inicio
        "'DIAS' TEXT," + //4; dias
        "'HORAS' TEXT," +  //5; horas
        "'USUARIO_ID' INTEGER);");  //6; usuarioId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists){
        String sql = "DROP TABLE" + (ifExists ? "IF EXISTS" : "") + "'TRATAMIENTO'";
        db.execSQL(sql);
    }

    @Override
    protected void bindValues(SQLiteStatement stmt, Tratamiento entity) {
        stmt.clearBindings();

        Long tratamientoId = entity.getTrataminetoId();
        if(tratamientoId != null){
            stmt.bindLong(1, tratamientoId);
        }

        String nombre_tratamiento = entity.getNombre_tratamiento();
        if(nombre_tratamiento != null){
            stmt.bindString(2, nombre_tratamiento);
        }

        String medicamento = entity.getMedicamento();
        if(medicamento != null){
            stmt.bindString(3, medicamento);
        }

        String fecha_inicio = entity.getFecha_inicio();
        if(fecha_inicio != null){
            stmt.bindString(4, fecha_inicio);
        }

        String dias = entity.getDias();
        if(dias != null){
            stmt.bindString(5, String.valueOf(dias));
        }

        String horas = entity.getHoras();
        if(horas != null){
            stmt.bindString(6, String.valueOf(horas));
        }

        Long usuarioId = entity.getUsuarioId();
        if(usuarioId != null){
            stmt.bindLong(7, usuarioId);
        }
    }

    @Override
    protected Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }

    @Override
    protected Tratamiento readEntity(Cursor cursor, int offset) {
        Tratamiento entity = new Tratamiento(
                cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), //tratamientoId
                cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), //nombre_tratamiento
                cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), //medicamento
                cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), //fecha_inicio
                cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), //dias
                cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), //horas
                cursor.isNull(offset + 6) ? null : cursor.getLong(offset + 6) //usuarioId
        );
        return entity;
    }

    @Override
    protected Long updateKeyAfterInsert(Tratamiento entity, long rowId) {
        entity.setTrataminetoId(rowId);
        return rowId;
    }

    @Override
    protected void readEntity(Cursor cursor, Tratamiento entity, int offset) {
        entity.setTrataminetoId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setNombre_tratamiento(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setMedicamento(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setFecha_inicio(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setDias(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setHoras(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setUsuarioId(cursor.isNull(offset + 6) ? null : cursor.getLong(offset + 6));
    }

    @Override
    protected Long getKey(Tratamiento entity) {
        if(entity != null) {
            return entity.getTrataminetoId();
        } else {
            return null;
        }
    }

    @Override
    protected boolean isEntityUpdateable() {
        return true;
    }

    public List<Tratamiento> _queryUsuario_Tratamientos(Long usuarioId) {
        synchronized (this) {
            if (usuario_TratamientosQuery == null) {
                QueryBuilder<Tratamiento> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.UsuarioId.eq(null));
                usuario_TratamientosQuery = queryBuilder.build();
            }
        }
        Query<Tratamiento> query = usuario_TratamientosQuery.forCurrentThread();
        query.setParameter(0, usuarioId);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getUsuarioDao().getAllColumns());
            builder.append(" FROM TRATAMIENTO T");
            builder.append(" LEFT JOIN USUARIO T0 ON T.'USUARIO_ID'=T0.'USUARIO_ID'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }

    protected Tratamiento loadCurrentDeep(Cursor cursor, boolean lock) {
        Tratamiento entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        Usuario usuario = loadCurrentOther(daoSession.getUsuarioDao(), cursor, offset);
        entity.setUsuario(usuario);

        return entity;
    }

    public Tratamiento loadDeep(Long key){
        assertSinglePk();
        if(key == null){
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();

        String[] keyArray = new String[] {key.toString()};
        Cursor cursor = db.rawQuery(sql, keyArray);

        try{
            boolean available = cursor.moveToFirst();
            if(!available){
                return null;
            }else if(!cursor.isLast()){
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        }finally {
            cursor.close();
        }
    }

    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Tratamiento> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<Tratamiento> list = new ArrayList<Tratamiento>(count);

        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }

    protected List<Tratamiento> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Tratamiento> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
}
