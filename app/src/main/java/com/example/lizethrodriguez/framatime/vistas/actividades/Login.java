package com.example.lizethrodriguez.framatime.vistas.actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Toast;

import com.example.lizethrodriguez.framatime.R;
import com.example.lizethrodriguez.framatime.entidades.FarmaTimeDB;
import com.example.lizethrodriguez.framatime.entidades.Usuario;
import com.example.lizethrodriguez.framatime.repositorios.UsuarioRepositorio;

public class Login extends AppCompatActivity {

    private AppCompatButton btn_login;
    private AppCompatTextView tv_link;
    private AppCompatEditText et_usuario, et_contrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        FarmaTimeDB.initializeInstance();

        this.et_usuario = (AppCompatEditText)findViewById(R.id.et_usuario);
        this.et_contrasena = (AppCompatEditText)findViewById(R.id.et_contrasena);
        this.btn_login = (AppCompatButton)findViewById(R.id.btn_login);
        this.tv_link = (AppCompatTextView)findViewById(R.id.tv_link);

        this.btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarTextos()){
                    UsuarioRepositorio usuarioRepositorio = new UsuarioRepositorio(Login.this);
                    Usuario usuario = usuarioRepositorio.iniciarSesion(et_usuario.getText().toString(), et_contrasena.getText().toString());
                    if(usuario != null){
                        startActivity(new Intent(Login.this, Menu.class));
                        Toast.makeText(Login.this, "Bienvenido", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else{
                        Toast.makeText(Login.this, "Usaurio o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        this.tv_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Registro.class));
            }
        });
    }

    public boolean validarTextos(){
        boolean textosValidos = true;

        if(et_usuario.getText().toString().length() == 0){
            et_usuario.setError("Requerido");
            textosValidos = false;
        }

        if(et_contrasena.getText().toString().length() == 0){
            et_contrasena.setError("Requerido");
            textosValidos = false;
        }
        return textosValidos;
    }


}
