package com.example.lizethrodriguez.framatime.vistas.actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;

import com.example.lizethrodriguez.framatime.R;

public class Menu extends AppCompatActivity {

    private AppCompatImageView btn_configuracion;
    private AppCompatButton btn_nuevo_medicamento, btn_mis_medicamentos, btn_historial_medico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        this.btn_configuracion = (AppCompatImageView) findViewById(R.id.btn_configuracion);
        this.btn_nuevo_medicamento = (AppCompatButton)findViewById(R.id.btn_nuevo_medicamento);
        this.btn_mis_medicamentos = (AppCompatButton) findViewById(R.id.btn_mis_medicamentos);
        this.btn_historial_medico = (AppCompatButton) findViewById(R.id.btn_historial_medico);

        this.btn_nuevo_medicamento.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, Nuevo_Medicamento.class));
            }
        });

        this.btn_mis_medicamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, Mis_Medicamentos.class));
            }
        });

        this.btn_historial_medico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, Historial_Medico.class));
            }
        });

        this.btn_configuracion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Menu.this, Configuraciones.class));
            }
        });

    }
}
