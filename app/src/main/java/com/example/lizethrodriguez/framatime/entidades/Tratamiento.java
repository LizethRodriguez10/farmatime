package com.example.lizethrodriguez.framatime.entidades;

import java.util.List;

import de.greenrobot.dao.DaoException;


/**
 * Created by Lizeth Rodriguez on 28/11/2016.
 */

public class Tratamiento {

    private Long trataminetoId;
    private String nombre_tratamiento;
    private String medicamento;
    private String fecha_inicio;
    private String dias;
    private String horas;
    private Long usuarioId;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient TratamientoDao myDao;

    private List<Tratamiento> tratamientos;

    private Usuario usuario;
    private Long usuario__resolvedKey;

    public Tratamiento(){
    }

    public Tratamiento(Long trataminetoId){this.trataminetoId = trataminetoId;}

    public Tratamiento(Long tratamientoId, String nombre_tratamiento, String medicamento, String fecha_inicio, String dias, String horas, Long usuarioId){
        this.trataminetoId = tratamientoId;
        this.nombre_tratamiento = nombre_tratamiento;
        this.medicamento = medicamento;
        this.fecha_inicio = fecha_inicio;
        this.dias = dias;
        this.horas = horas;
        this.usuarioId = usuarioId;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession){
        this.daoSession = daoSession;
        myDao = daoSession !=null ? daoSession.getTratamientoDao():null;
    }

    public Long getTrataminetoId(){return trataminetoId;}
    public void setTrataminetoId(Long trataminetoId){this.trataminetoId = trataminetoId;}

    public String getNombre_tratamiento(){return nombre_tratamiento;}
    public void setNombre_tratamiento(String nombre_tratamiento){this.nombre_tratamiento = nombre_tratamiento;}

    public String getMedicamento(){return medicamento;}
    public void setMedicamento(String medicamento){this.medicamento = medicamento;}

    public String getFecha_inicio(){return fecha_inicio;}
    public void setFecha_inicio(String fecha_inicio){this.fecha_inicio = fecha_inicio;}

    public String getDias(){return dias;}
    public void setDias(String dias){this.dias = dias;}

    public String getHoras(){return horas;}
    public void setHoras(String horas){this.horas = horas;}

    public Long getUsuarioId(){return usuarioId;}
    public void setUsuarioId(Long usuarioId){this.usuarioId = usuarioId;}

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<Tratamiento> getCoordenadas() {
        if (tratamientos == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            TratamientoDao targetDao = daoSession.getTratamientoDao();
            List<Tratamiento> coordenadasNew = targetDao._queryUsuario_Tratamientos(usuarioId);
            synchronized (this) {
                if(tratamientos == null) {
                    tratamientos = coordenadasNew;
                }
            }
        }
        return tratamientos;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetCoordenadas() {
        tratamientos = null;
    }

    /** To-one relationship, resolved on first access. */
    public Usuario getUsuario(){
        Long __key = this.usuarioId;
        if(usuario__resolvedKey == null || !usuario__resolvedKey.equals(__key)){
            if(daoSession == null){
                throw new DaoException("Entity is detached from DAO context");
            }
            UsuarioDao targetDao = daoSession.getUsuarioDao();
            Usuario usuarioNew = targetDao.load(__key);
            synchronized (this){
                usuario = usuarioNew;
                usuario__resolvedKey = __key;
            }
        }
        return usuario;
    }

    public void setUsuario(Usuario usuario){
        synchronized (this){
            this.usuario = usuario;
            usuarioId = usuario == null ? null : usuario.getUsuarioId();
            usuario__resolvedKey = usuarioId;
        }
    }


    public void delete(){
        if(myDao == null){
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }


    public void update(){
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }


    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
}


