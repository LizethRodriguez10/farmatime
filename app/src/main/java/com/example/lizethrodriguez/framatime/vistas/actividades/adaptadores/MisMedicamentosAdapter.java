package com.example.lizethrodriguez.framatime.vistas.actividades.adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by savidsalazar on 16/11/16.
 */

public class MisMedicamentosAdapter extends RecyclerView.Adapter<MisMedicamentosAdapter.ViewHolder> {

    private List<String> items;
    private Context context;
    private int viewId;
    //private UsuarioRepositorio usuarioRepositorio;

    public MisMedicamentosAdapter(Context context, int viewResourceId, List<String> prItems) {
        this.items = prItems;
        this.context = context;
        this.viewId = viewResourceId;
        //this.usuarioRepositorio = new UsuarioRepositorio(context);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(viewId, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final String usuario = items.get(i);
        /*
        viewHolder.txtNombre.setText(usuario.getNombre() + " " + usuario.getApellidoPaterno());
        viewHolder.txtLatitudLongitud.setText(usuario.getCorreo());

        viewHolder.btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long usuarioID = usuario.getUsuarioId();
            }
        });

        viewHolder.btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Long usuarioID = usuario.getUsuarioId();
                usuarioRepositorio.eliminarUsuario(usuario);
                items.remove(usuario);
                notifyDataSetChanged();
            }
        });*/

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        /*private TextView txtNombre, txtLatitudLongitud;
        private Button btnEditar, btnEliminar;*/

        public ViewHolder(View itemView) {
            super(itemView);
            /*
            this.txtNombre = (TextView) itemView.findViewById(R.id.txtNombre);
            this.txtLatitudLongitud = (TextView) itemView.findViewById(R.id.txtLatitudLongitud);
            this.btnEditar = (Button) itemView.findViewById(R.id.btnEditar);
            this.btnEliminar = (Button) itemView.findViewById(R.id.btnEliminar);*/
        }
    }
}
