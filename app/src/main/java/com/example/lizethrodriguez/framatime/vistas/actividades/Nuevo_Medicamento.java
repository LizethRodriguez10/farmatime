package com.example.lizethrodriguez.framatime.vistas.actividades;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.lizethrodriguez.framatime.R;
import com.example.lizethrodriguez.framatime.entidades.Tratamiento;
import com.example.lizethrodriguez.framatime.repositorios.TratamientoRepositorio;

public class Nuevo_Medicamento extends AppCompatActivity {

    private AppCompatButton btn_regresar, btn_guardar;

    private AppCompatEditText et_tratamiento, et_medicamento, et_dia, et_mes, et_ano, et_dias, et_horas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuevo_medicamento);

        this.btn_regresar = (AppCompatButton)findViewById(R.id.btn_regresar);
        this.btn_guardar = (AppCompatButton)findViewById(R.id.btn_guardar);

        this.et_tratamiento = (AppCompatEditText)findViewById(R.id.et_tratamiento);
        this.et_medicamento = (AppCompatEditText)findViewById(R.id.et_medicamento);
        this.et_dia = (AppCompatEditText)findViewById(R.id.et_dia);
        this.et_mes = (AppCompatEditText)findViewById(R.id.et_mes);
        this.et_ano = (AppCompatEditText)findViewById(R.id.et_ano);
        this.et_dias = (AppCompatEditText)findViewById(R.id.et_num_dias);
        this.et_horas = (AppCompatEditText)findViewById(R.id.et_cada_hora);

        this.btn_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarTextos()){
                    Tratamiento tratamiento = new Tratamiento();
                    tratamiento.setNombre_tratamiento(et_tratamiento.getText().toString());
                    tratamiento.setMedicamento(et_medicamento.getText().toString());
                    tratamiento.setFecha_inicio(et_dia.getText().toString() +  "-" + et_mes.getText().toString() + "-" + et_ano.getText().toString());
                    tratamiento.setDias(et_dias.getText().toString());
                    tratamiento.setHoras(et_horas.getText().toString());
                    TratamientoRepositorio repositorio = new TratamientoRepositorio(Nuevo_Medicamento.this);
                    if(repositorio.guardarTratamiento(tratamiento)){
                        Toast.makeText(Nuevo_Medicamento.this, "Tratamiento registrado correctamente", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        });
    }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
             switch (item.getItemId()) {
                 // Respond to the action bar's Up/Home button
                 case android.R.id.home:
                      finish();
                      return true;
            }
            return super.onOptionsItemSelected(item);
        }

        public boolean validarTextos(){
            boolean textosValidos = true;
            if(et_tratamiento.getText().toString().length() == 0){
                et_tratamiento.setError("Requerido");
                textosValidos = false;
            }

            if(et_medicamento.getText().toString().length() == 0){
                et_medicamento.setError("Requerido");
                textosValidos = false;
            }

            if(et_dia.getText().toString().length() == 0){
                et_dia.setError("Requerido");
                textosValidos = false;
            }

            if(et_mes.getText().toString().length() == 0){
                et_mes.setError("Requerido");
                textosValidos = false;
            }

            if(et_ano.getText().toString().length() == 0){
                et_ano.setError("Requerido");
                textosValidos = false;
            }

            if(et_dias.getText().toString().length() == 0){
                et_dias.setError("Requerido");
                textosValidos = false;
            }

            if(et_horas.getText().toString().length() == 0){
                et_horas.setError("Requerido");
                textosValidos = false;
            }
            return textosValidos;
        }
}
