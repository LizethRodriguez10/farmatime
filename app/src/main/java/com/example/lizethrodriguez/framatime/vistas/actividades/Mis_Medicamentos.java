package com.example.lizethrodriguez.framatime.vistas.actividades;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.lizethrodriguez.framatime.R;
import com.example.lizethrodriguez.framatime.vistas.actividades.adaptadores.MisMedicamentosAdapter;

import java.util.ArrayList;

public class Mis_Medicamentos extends AppCompatActivity {


    private RecyclerView.Adapter mAdapter;
    private RecyclerView rv_tratamientos_saved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mis__medicamentos);

        this.rv_tratamientos_saved = (RecyclerView) findViewById(R.id.rv_tratamientos_saved);


        rv_tratamientos_saved.setHasFixedSize(true);
        rv_tratamientos_saved.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MisMedicamentosAdapter(this, R.layout.item_mi_medicamento, new ArrayList<String>());
        rv_tratamientos_saved.setAdapter(mAdapter);
    }
}
