package com.example.lizethrodriguez.framatime.repositorios;

import android.content.Context;
import android.util.Log;

import com.example.lizethrodriguez.framatime.entidades.DaoSession;
import com.example.lizethrodriguez.framatime.entidades.FarmaTimeDB;
import com.example.lizethrodriguez.framatime.entidades.Tratamiento;
import com.example.lizethrodriguez.framatime.entidades.TratamientoDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lizeth Rodriguez on 28/11/2016.
 */

public class TratamientoRepositorio {

    private static final String TAG = "Tratamiento";

    private Context context;

    public TratamientoRepositorio(Context prContext){this.context = prContext;}

    public boolean guardarTratamiento(Tratamiento prTratamiento){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            oTratamientoDao.insert(prTratamiento);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public boolean guardarOActualizarTratamiento(Tratamiento prTratamiento){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            oTratamientoDao.insertOrReplace(prTratamiento);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public boolean guardarOActualizarTratamiento(ArrayList<Tratamiento> prTratamiento){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            oTratamientoDao.insertOrReplaceInTx(prTratamiento);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public boolean eliminarTratamiento(Tratamiento prTratamiento){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            oTratamientoDao.delete(prTratamiento);
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public boolean eliminarTodo(){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            oTratamientoDao.deleteAll();
            return true;
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return false;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public List<Tratamiento> obtenerTratamientosPorUsuario(Long usuarioId){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            return oTratamientoDao.queryBuilder().where(TratamientoDao.Properties.UsuarioId.eq(usuarioId)).list();
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return null;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public List<Tratamiento> obtenerTratamiento(){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            return oTratamientoDao.loadAll();
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return null;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

    public Tratamiento obtenerTratamiento(Long prKey){
        try {
            DaoSession oFarmaTimeDB = FarmaTimeDB.getInstance().openDatabase(context);
            TratamientoDao oTratamientoDao = oFarmaTimeDB.getTratamientoDao();
            return oTratamientoDao.load(prKey);
        }
        catch (Exception error){
            Log.e(TAG, error.getMessage());
            return null;
        }
        finally {
            FarmaTimeDB.getInstance().closeDatabase();
        }
    }

}
