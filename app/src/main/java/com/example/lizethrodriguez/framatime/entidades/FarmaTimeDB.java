package com.example.lizethrodriguez.framatime.entidades;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by SavidSalazar
 */

public class FarmaTimeDB {
    private AtomicInteger mOpenCounter = new AtomicInteger();

    private static FarmaTimeDB instance;
    private static DaoMaster.DevOpenHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;
    private DaoSession daoSession;

    public static synchronized void initializeInstance() {
        if (instance == null) {
            instance = new FarmaTimeDB();
        }
    }

    public static synchronized FarmaTimeDB getInstance() {
        if (instance == null) {
            throw new IllegalStateException(FarmaTimeDB.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return instance;
    }

    public synchronized DaoSession getSession()
    {
        DaoMaster daoMaster = new DaoMaster(mDatabase);
        daoSession = daoMaster.newSession();
        return daoSession;
    }

    public synchronized DaoSession openDatabase(Context context) {
        if(mOpenCounter.incrementAndGet() == 1) {
            // Opening new database
            mDatabaseHelper = new DaoMaster.DevOpenHelper(context, "FarmaTime-db", null);
            mDatabase = mDatabaseHelper.getWritableDatabase();
            DaoMaster daoMaster = new DaoMaster(mDatabase);
            daoSession = daoMaster.newSession();
        }
        return daoSession;
    }

    public synchronized void closeDatabase() {
        if(mOpenCounter.decrementAndGet() == 0) {
            // Closing database
            mDatabase.close();
            daoSession.clear();

        }
    }
}