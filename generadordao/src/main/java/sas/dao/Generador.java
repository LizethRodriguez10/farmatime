package sas.dao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class Generador {

    public static void main(String args[]) throws Exception {

        String packageName = "com.example.lizethrodriguez.farmatime";
        String dbName = "FarmaTime";

        PlantillaGenerador oPlantillaGenerador = new PlantillaGenerador(dbName,packageName,args[0]);
        Schema schema = new Schema(1, packageName + ".entidades");

        //Tabla de usuario
        Entity usuario = schema.addEntity("Usuario");
        usuario.addLongProperty("usuarioId").primaryKey().autoincrement();
        usuario.addStringProperty("nombre");
        usuario.addStringProperty("correo");
        usuario.addStringProperty("contra");
        oPlantillaGenerador.generarController("Usuario", false);

        //Tabla de tratamiento
        Entity tratamiento = schema.addEntity("Tratamiento");
        tratamiento.addLongProperty("tratamientoId").primaryKey().autoincrement();
        tratamiento.addStringProperty("nombre_tratamiento");
        tratamiento.addStringProperty("fecha_inicio");
        tratamiento.addIntProperty("dias");
        tratamiento.addIntProperty("horas");
        oPlantillaGenerador.generarController("Tratamiento", false);

        Property idUsuario = tratamiento.addLongProperty("usuarioId").getProperty();
        ToMany tratamientos = usuario.addToMany(tratamiento, idUsuario);
        tratamientos.setName("tratamientos");
        tratamiento.addToOne(usuario, idUsuario);

        //Tabla de medicamento

        //Tabla Cliente
        /*
        Entity cliente = schema.addEntity("Cliente");
        cliente.addStringProperty("clienteId").primaryKey();
        cliente.addStringProperty("cliente_observacion_id");
        cliente.addStringProperty("calle");
        cliente.addStringProperty("numeroExterior");
        cliente.addStringProperty("numeroInterior");
        cliente.addStringProperty("zona");
        cliente.addStringProperty("nombreCliente");
        cliente.addLongProperty("cargaTrabajo");
        cliente.addDoubleProperty("latitud");
        cliente.addDoubleProperty("longitud");
        oPlantillaGenerador.generarController("Cliente", false);

        //Relacion clientes usuario
        Property usuarioId = cliente.addLongProperty("usuarioId").getProperty();
        ToMany clientes = usuario.addToMany(cliente, usuarioId);
        clientes.setName("clientes");
        cliente.addToOne(usuario, usuarioId);*/


        new DaoGenerator().generateAll(schema, args[0]);
        oPlantillaGenerador.generarDB();
    }


}
